#!/usr/bin/env bash

set -ueo pipefail

usage() {
cat << EOF
Un pluging de pruebas que revisa la estructura, ver https://github.com/helm/helm/blob/master/docs/plugins.md.

Comandos disponibles:
  help    	Muestra esta ayuda
  create    Crea un pluging

EOF
}


case "$1" in
  help)
    usage
  ;;
  stop)
    ls -l
  ;;
  del)
    echo "delete all recipes"
  ;;
  update)
    echo "update all plugins $HELM_PLUGIN_DIR"
    cd $HELM_PLUGIN_DIR ; git pull 
  ;;
  reload|restart|force-reload)
    ls
  ;;
  *)
    usage
  ;;
esac
